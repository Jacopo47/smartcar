#include "Led.h"
#include "Arduino.h"

Led::Led(int pin){
  this->pin = pin;
  pinMode(pin,OUTPUT);
  state = false;
}

void Led::switchOn(){
  digitalWrite(pin,HIGH);
  state = true;
}

void Led::switchOff(){
  digitalWrite(pin,LOW);
  state = false;
}

bool Led::getState() {
  return state;
}

void Led::fade() {
  analogWrite(pin, currIntensity);
  currIntensity = currIntensity + fadeAmount;
  if (currIntensity == 0 || currIntensity == 255) {
  fadeAmount = -fadeAmount ;
  }
};
