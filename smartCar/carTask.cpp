#include "MsgService.h"
#include "carTask.h"
#include "SoftwareSerial.h"
#include "ServoTimer2.h"
#include <Wire.h>
#define LED1 9
#define LED2 10
#define BUTTON 8
#define TIME 70
const float dMin = 2.5;
const float dMax = 5.0;

carTask::carTask(BTask* bt) {
    butt = new ButtonImpl(BUTTON);
    this->bt = bt;
    servo = new ServoTimer2();
    servo->attach(11);
    prox = new Proximity();
    led1 = new Led(LED1);
    led2 = new Led(LED2);
    checkTime = 0;
    checkPosizione = false;
    checkLed2 = false;
    i = 0;
    checkBanana = false;
    checkOlio = false;
    checkMissile = false;
}

void carTask::reset() {
  led1->switchOff();
  led2->switchOff();
  checkTime = 0;
  checkPosizione = false;
  checkLed2 = false;
  i = 0;
  servo->write(0);
}

void carTask::resetBattle() {
    checkBanana = false;
    checkMissile = false;
    checkOlio = false;
}

void carTask::tick(String carState, String battle) {
        float app;
        if(this->carState != carState) {
          reset();
          resetBattle();
        }
        if(this->battleState != battle) {
            resetBattle();
        }
        
        if(carState == "ACCESA") {
                
                if(!led1->getState()) {
                    led1->switchOn();
                }
                app = prox->getDistance();
                if (app < dMax) {
                    bt->sendMsg("Presenza veicolo" + String(app));
                    
                    if  (prox->getDistance() < dMin) {
                        led2->switchOn();
                    } else {
                        led2->switchOff();
                    }
                } 
               

                if (butt->isPressed()) {
                    bt->sendMsg("Contatto");
               } 
        } else if(carState == "PARCHEGGIO") {
                led1->fade();
                
                if(butt->isPressed()) {
                    if(!led2->getState()) {
                        led2->switchOn();
                        checkLed2 = true;
                    } 


           
                    if (!checkPosizione) {
                        bt->sendMsg("POSIZIONE");
                        checkPosizione = true;
                    }
                }

                if (checkLed2) {
                    if (i > TIME) {
                        i = 0;
                        checkLed2 = false;
                        led2->switchOff();
                    } else {
                      i++;
                    }
                }


            }
        

        if (battle == "BUCCE") {
                if(!checkBanana) {
                    Serial.println("LANCIO BUCCE DI BANANE");
                    servo->write(1000);
                    checkBanana = true;
                }
        } else if (battle == "MISSILI") {
              
                if(!checkMissile) {
                  Serial.println("LANCIO MISSILI");
                    servo->write(2000);
                    checkMissile = true;
                }
        } else if (battle == "OLIO") {
              
                if(!checkOlio) {
                    Serial.println("RILASCIO OLIO");
                    servo->write(1500);
                    checkOlio = true;
                }
        } else if (battle == "PACE") {
                if(servo->read()!= 0) {
                    servo->write(0);
                    resetBattle();
                }
        }

        this->carState = carState;
        this->battleState = battle;
}
