/*
 * Proximity.h
 *
 *  Created on: 03 nov 2016
 *      Author: jacopo
 */

#ifndef PROXIMITY_H_
#define PROXIMITY_H_



class Proximity {
public:
	Proximity();
	float getDistance();
};


#endif /* PROXIMITY_H_ */
