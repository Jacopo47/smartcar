#ifndef __BTASK__
#define __BTASK__

#include "Arduino.h"
#include "SoftwareSerial.h"


class BTask {
    
    public:
        BTask();
        
        void tick();
        
        String getBattleState();
        String getCarState();
        void sendMsg(String msg);
    private:
        String carState;
        String battle;
};



#endif
