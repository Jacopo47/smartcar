/*
 * Proximity.cpp
 *
 *  Created on: 03 nov 2016
 *      Author: jacopo
 */

#include "Proximity.h"
#include "Arduino.h"

#define trigPin 4
#define echoPin 5

const float vs = 331.5 + 0.6 * 20;

Proximity::Proximity() {
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
}

float Proximity::getDistance() {
	digitalWrite(trigPin, LOW);
	delayMicroseconds(3);
	digitalWrite(trigPin, HIGH);
	delayMicroseconds(5);
	digitalWrite(trigPin, LOW);

	float tUS = pulseIn(echoPin, HIGH);
	float t = tUS / 100.0 / 1000.0 / 2;
	float d = t * vs;
	return d;
}



