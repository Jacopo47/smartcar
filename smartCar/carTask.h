#ifndef __CARTASK__
#define __CARTASK__

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "Led.h"
#include "ButtonImpl.h"
#include "BTask.h"
#include "ServoTimer2.h"
#include "Proximity.h"

class carTask {

    public: 
        carTask(BTask* bt);
        void tick(String carState, String battle);
        void reset();
        void resetBattle();
    private:
        String carState;
        Led* led1;
        Led* led2;
        Proximity* prox;
        Button* butt;
        BTask* bt;
        int checkTime;
        bool checkPosizione;
        ServoTimer2* servo;
        bool checkLed2;
        int i;
        bool checkBanana;
        bool checkOlio;
        bool checkMissile;
        String battleState;
        
};

#endif
