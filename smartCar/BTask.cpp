#include "MsgService.h"
#include "BTask.h"
#include "SoftwareSerial.h"
#include <Wire.h>

MsgService msgService(2,3);


BTask::BTask() {
  msgService.init();
}

void BTask::tick() {
 
  if (msgService.isMsgAvailable()) {
    Msg* message = msgService.receiveMsg();
    String msg = message->getContent();
    if (msg == "ACCENDI") {
      carState = "ACCESA";
      battle = "PACE";
    } else if (msg == "PARCHEGGIO") {
               carState = "PARCHEGGIO";
               battle = "PACE";
             } else if(msg == "SPENTA") {
               carState = "SPENTO";
               battle = "PACE";
             }else if(msg == "BUCCE") {
               battle = "BUCCE";
             }else if(msg == "MISSILI") {
               battle = "MISSILI";
             }else if(msg == "OLIO") {
               battle = "OLIO";
             } else {
               carState = "default";
             }
        }
    }

void BTask::sendMsg(String msg) {               
  msgService.sendMsg(Msg(msg));
}

String BTask::getCarState() {
  return carState;
}

String BTask::getBattleState() {
  return battle;
}
