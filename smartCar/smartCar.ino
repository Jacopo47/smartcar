#include "BTask.h"
#include "carTask.h"

BTask* bluetooth;
carTask* car;

void setup() {
  Serial.begin(9600);
  Serial.println("Inizio");
  Serial.flush();
  bluetooth = new BTask();
  car = new carTask(bluetooth);
  
}

void loop() {
  bluetooth->tick();
  car->tick(bluetooth->getCarState(), bluetooth->getBattleState()); 
  delay(30);
}
