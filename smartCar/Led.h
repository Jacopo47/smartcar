#ifndef __LED__
#define __LED__

#include "Light.h"

class Led: public Light { 
public:
  Led(int pin);
  void switchOn();
  void switchOff();
  void fade();  
  bool getState();  
private:
  int pin;  
  bool state;
  int currIntensity = 0;
  int fadeAmount = 5;
};

#endif
