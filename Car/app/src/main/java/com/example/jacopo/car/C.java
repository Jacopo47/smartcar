package com.example.jacopo.car;

/**
 * Created by jacopo on 04/12/16.
 */

public class C {
    public static final String LOG_TAG = "SmartCar";

    static final String TARGET_BT_DEVICE_NAME = "ARDUJ";
    static final String TARGET_BT_DEVICE_UUID = "00001101-0000-1000-8000-00805F9B34FB";

    static final int ENABLE_BT_REQUEST = 1;

    static final String DEFAULT_MOVIMENTO = "In movimento..";
    static final String DEFAULT_PARCHEGGIO = "Ferma in parcheggio..";
    static final String DEFAULT_SPENTA = "Spenta..";

    static final String MOVIMENTO_VICINANZA = "PRESENZA VEICOLO - DISTANZA: ";
    static final String CONTATTO = "CONTATTO!";

    static final String IN_VICINANZA = "Presenza veicolo";
    static final String MV_CONTATTO = "Contatto";
    static final String PARK_CONTATTO = "POSIZIONE";

    static final String START = "ACCENDI";
    static final String PARK = "PARCHEGGIO";
    static final String STOP = "SPENTA";

}
