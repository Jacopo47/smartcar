package com.example.jacopo.car.bt;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Set;

/**
 * Created by jacopo on 06/12/16.
 */

public class BluetoothUtils {

    public static BluetoothDevice findPairedDevice(String deviceName, BluetoothAdapter adapter) {
        BluetoothDevice targetDevice = null;

        Set<BluetoothDevice> pairedList = adapter.getBondedDevices();

        if (pairedList.size()>0) {
            for (BluetoothDevice device : pairedList) {
                if(device.getName().equals(deviceName)) {
                    targetDevice = device;
                }
            }
        }

        return targetDevice;
    }

}
