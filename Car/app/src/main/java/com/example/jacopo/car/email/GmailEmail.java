package com.example.jacopo.car.email;

import java.util.List;

/**
 * Created by jacopo on 14/12/16.
 */

public class GmailEmail {

    private List<String> toEmailList;
    private String emailSubject;
    private String emailBody;
    public GmailEmail(String emailSubject, String emailBody, List<String> toEmailList){
        this.toEmailList = toEmailList;
        this.emailSubject = emailSubject;
        this.emailBody = emailBody;
    }
    public String getBody(){
        return this.emailBody;
    }
    public String getSubject(){
        return this.emailSubject;
    }
    public List<String> getRecipients(){
        return this.toEmailList;
    }
}
