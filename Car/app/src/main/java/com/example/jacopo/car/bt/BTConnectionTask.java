package com.example.jacopo.car.bt;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.jacopo.car.MainActivity;
import com.example.jacopo.car.R;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by jacopo on 04/12/16.
 */

public class BTConnectionTask extends AsyncTask<Void, Void, Boolean> {

    private BluetoothSocket btSocket = null;
    private MainActivity context = null;

    public BTConnectionTask(MainActivity context, BluetoothDevice server, UUID uuid) {
        this.context = context;

        try {
            btSocket = server.createRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            btSocket.connect();
        } catch (IOException connectException) {
            try {
                btSocket.close();
            } catch (IOException closeException) {
                closeException.printStackTrace();
            }
            connectException.printStackTrace();
            return  false;
        }

        BTConnectionManager connectionManager = BTConnectionManager.getInstance();
        connectionManager.setChannel(btSocket);
        connectionManager.start();

        return true;
    }

    protected void onPostExecute(Boolean connected) {
        TextView flagLabel = (TextView) context.findViewById(R.id.btConnection);

        if(connected){
            flagLabel.setText("BT Status: Connected");
        } else {
            flagLabel.setText("BT Status: Not Connected");
        }
    }
}
