package com.example.jacopo.car;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.jacopo.car.bt.BTConnectionManager;
import com.example.jacopo.car.bt.MsgTooBigException;

public class ChoiceActivity extends AppCompatActivity {

    private ProgressBar bar;
    private Button banana, oil, rocket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initUI();
    }

    private void initUI() {

        bar = (ProgressBar) findViewById(R.id.progressBar2);
        bar.setMax(180);
        banana = (Button) findViewById(R.id.Banane);

        banana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bar.setProgress(60);
                try {
                    BTConnectionManager.getInstance().sendMsg("BUCCE");
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }

            }
        });
        oil = (Button) findViewById(R.id.Olio);

        oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bar.setProgress(120);
                try {
                    BTConnectionManager.getInstance().sendMsg("OLIO");
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });
        rocket = (Button) findViewById(R.id.missili);

        rocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bar.setProgress(180);

                try {

                    BTConnectionManager.getInstance().sendMsg("MISSILI");

                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



}
