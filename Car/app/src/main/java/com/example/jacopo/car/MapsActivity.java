package com.example.jacopo.car;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.jacopo.car.email.GmailClient;
import com.example.jacopo.car.email.GmailEmail;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationManager lm;
    private Marker prevMark;
    private String check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        check = getIntent().getStringExtra("sendMail");
        this.sendMail();
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        if(prevMark != null) {
            prevMark.remove();
        }

        this.checkPermission();
        Location pos = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (pos != null ) {
            LatLng curr = new LatLng(pos.getLatitude(), pos.getLongitude());
            prevMark = mMap.addMarker(new MarkerOptions().position(curr).title("La tua auto è qui"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curr, mMap.getMaxZoomLevel() - 4));
        } else {
            LatLng curr = new LatLng(-34,151);
            prevMark = mMap.addMarker(new MarkerOptions().position(curr).title("La tua auto è qui"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curr, mMap.getMaxZoomLevel() - 4));
        }

    }

    private void checkPermission() {
        final int ACCESS_FINE_LOCATION_REQUEST = 1234;
        int permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(permission == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                    }, ACCESS_FINE_LOCATION_REQUEST);
        }

        final int ACCESS_COARSE_LOCATION_REQUEST = 1235;
        permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if(permission == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {
                            android.Manifest.permission.ACCESS_COARSE_LOCATION
                    }, ACCESS_COARSE_LOCATION_REQUEST);
        }
    }

    private void sendMail() {

       
        if(check.equals("YES")) {
            Thread t = new Thread() {
                @Override
                public void run() {

                    checkPermission();
                    Location l = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    try {
                        String fromUsername = "pse.student.unibo@gmail.com";

                        String fromPassword = "pse.student";

                        List<String> toAddress = new ArrayList<>(Arrays.asList(new String[]{"jacopo.riciputi@gmail.com"}));

                        String mailSubject = "Incidente localizzato";

                        String mailBody = "Salve\n Il suo veicolo ha riscontrato un incidente presso: " +
                                l.getLatitude()+ ", " + l.getLongitude();

                        GmailEmail email = new GmailEmail(mailSubject, mailBody, toAddress);

                        GmailClient client = new GmailClient(fromUsername,fromPassword);

                        client.sendEmail(email);
                    } catch (UnsupportedEncodingException | MessagingException e) {

                        e.printStackTrace();

                    }
                }
            };
            t.start();
            
        }
    }
}
