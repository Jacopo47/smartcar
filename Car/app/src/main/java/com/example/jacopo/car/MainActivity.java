package com.example.jacopo.car;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.jacopo.car.bt.BTConnectionManager;
import com.example.jacopo.car.bt.BTConnectionTask;
import com.example.jacopo.car.bt.BluetoothUtils;
import com.example.jacopo.car.bt.MsgTooBigException;
import com.google.android.gms.common.api.GoogleApiClient;

import java.lang.ref.WeakReference;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private TextView display;
    private ToggleButton start, park, stop;
    private Switch notify;
    private boolean check = true;

    private static BluetoothAdapter btAdapter;
    private static BluetoothDevice targetDevice;
    public static boolean state = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private static MainActivityHandler uiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        initUI();

        uiHandler = new MainActivityHandler(this);

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null) {
            if (btAdapter.isEnabled()) {
                targetDevice = BluetoothUtils.findPairedDevice(C.TARGET_BT_DEVICE_NAME, btAdapter);

                if (targetDevice != null) {
                    ((TextView) findViewById(R.id.btDeviceName)).setText("BT Device: Found " + targetDevice.getName());
                    connectToTargetBtDevice();
                }
            } else {
                startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.ENABLE_BT_REQUEST);
            }
        } else {
            showBluetoothUnavailableAlert();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        BTConnectionManager.getInstance().cancel();
    }

    public void onActivityResult(int reqID, int res, Intent data) {
        if (reqID == C.ENABLE_BT_REQUEST && res == Activity.RESULT_OK) {
            targetDevice = BluetoothUtils.findPairedDevice(C.TARGET_BT_DEVICE_NAME, btAdapter);

            if (targetDevice != null) {
                ((TextView) findViewById(R.id.btDeviceName)).setText("BT Device: Found " + targetDevice.getName());
                connectToTargetBtDevice();
            }
        }

    }

    private void initUI() {
        display = (TextView) findViewById(R.id.Display);

        start = (ToggleButton) findViewById(R.id.bAccesa);
        park = (ToggleButton) findViewById(R.id.bParcheggio);
        stop = (ToggleButton) findViewById(R.id.bSpenta);

        notify = (Switch) findViewById(R.id.Notifica);
        notify.setChecked(true);

        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                turnOnCar();
            }
        });

        park.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                parkCar();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                stopCar();
            }
        });

        notify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    check = true;
                } else {
                    check = false;
                }
            }
        });



    }

    private void turnOnCar() {
        try {
            BTConnectionManager.getInstance().sendMsg(C.START);
            display.setText(C.DEFAULT_MOVIMENTO);
            park.setChecked(false);
            state = true;
            stop.setChecked(false);
        } catch (MsgTooBigException e) {
            e.printStackTrace();
        }
    }

    private void parkCar() {
        try {
            BTConnectionManager.getInstance().sendMsg(C.PARK);
            display.setText(C.DEFAULT_PARCHEGGIO);
            start.setChecked(false);
            stop.setChecked(false);
            state = true;

        } catch (MsgTooBigException e) {
            e.printStackTrace();
        }
    }

    private void stopCar() {
        try {
            BTConnectionManager.getInstance().sendMsg(C.STOP);
            display.setText(C.DEFAULT_SPENTA);
            start.setChecked(false);
            park.setChecked(false);
            state = true;
        } catch (MsgTooBigException e) {
            e.printStackTrace();
        }
    }

    public void moveContact() {
        Intent intent = new Intent(getApplicationContext(), ChoiceActivity.class);
        startActivity(intent);
    }

    public void parkContact() {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        if (check) {
            intent.putExtra("sendMail", "YES");
        } else {
            intent.putExtra("sendMail", "NO");
        }
        startActivity(intent);
    }



    public void near(Double d) {
        display.setText(C.MOVIMENTO_VICINANZA + " " + d);
    }




    private void showBluetoothUnavailableAlert() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Warning")
                .setMessage("Bluetooth unaviable on this device")
                .setCancelable(false)
                .setNeutralButton("Close APP", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        MainActivity.this.finish();
                    }
                })
                .create();

        dialog.show();
    }

    private void connectToTargetBtDevice() {
        UUID uuid = UUID.fromString(C.TARGET_BT_DEVICE_UUID);

        BTConnectionTask task = new BTConnectionTask(this, targetDevice, uuid);

        task.execute();

    }

    public static MainActivityHandler getHandler() {
        return uiHandler;
    }

    public static class MainActivityHandler extends Handler {
        private final WeakReference<MainActivity> context;

        MainActivityHandler(MainActivity context) {
            this.context = new WeakReference<>(context);
        }

        public void handleMessage(Message msg) {
            Object obj = msg.obj;

            if(obj instanceof String) {
                String message = obj.toString();

                switch (message) {
                    case C.MV_CONTATTO:
                        Log.d(C.LOG_TAG, "CONTATTO");
                        context.get().moveContact();
                        break;
                    case C.PARK_CONTATTO:
                        context.get().parkContact();
                        break;


                    default:
                        if (!MainActivity.state) {
                            if (message.contains(C.IN_VICINANZA)) {
                                context.get().near(Double.parseDouble(message.replace(C.IN_VICINANZA, "")));
                            }
                        } else {
                            MainActivity.state = false;
                        }
                        break;
                }
            }
        }
    }
}
